function goTo(direction) {
	if ('allp' === direction) {
		window.location.href = "/slim/all-posts";
	} else if ('addp' === direction) {
		window.location.href = "/slim/add-post";
	} else {
		window.location.href = "/slim/";
	}
}
