# Slim application 
This application is developed by Genet Schneider as a part of his thesis work.
This application uses [Slim](https://www.slimframework.com/) PHP microframework. 

# Install
Apache server is required to run this application. Place this in htdocs/slim.
localhost:8888/slim should work. Author uses MAMP.
Use `database-dump.sql` for database reference. This app needs the tables, documented in the file.