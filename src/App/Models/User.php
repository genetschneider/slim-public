<?php

declare(strict_types=1);

namespace SlimApp\App\Models;

class User
{
	protected $name;

	// will never be inserted to db
	protected $id;

	public function __construct(string $name, int $id = 0)
	{
		$this->name = $name ?? 'unknown';
		$this->id = $id;
	}

	/**
	 * Getter for name
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * Getter for the id
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * Method to return the insert array for user
	 * @return array
	 */
	public function getInsertArray(): array
	{
		return [
			'name' => $this->getName()
		];
	}

	/**
	 * Method to return this entity as array
	 * @return array
	 */
	public function toArray(): array
	{
		return (array)$this;
	}
}
