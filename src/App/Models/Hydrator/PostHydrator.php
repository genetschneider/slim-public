<?php

declare(strict_types = 1);

namespace SlimApp\App\Models\Hydrator;

use SlimApp\App\Models\Post;

class PostHydrator
{
	/**
	 * @param array $postArray
	 * @return Post
	 */
	public function hydrate(array $postArray): Post
	{
		$post = new Post(
			$postArray['title'] ?? '',
			$postArray['content'] ?? '',
			(int)$postArray['written_by'] ?? 0
		);

		return $post;
	}
}
