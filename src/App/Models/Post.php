<?php

declare(strict_types=1);

namespace SlimApp\App\Models;

class Post
{
	protected $title;
	protected $content;
	protected $userId;

	public function __construct(string $title, string $content, int $userId)
	{
		$this->title = $title ?? 'unknown';
		$this->content = $content ?? 'unknown';
		$this->userId = $userId ?? 1;
	}

	/**
	 * Getter for title
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * Getter for content
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * getter for userId
	 * @return int
	 */
	public function getUserId(): int
	{
		return $this->userId;
	}

	/**
	 * Method to return the insert array for post
	 * @return array
	 */
	public function getInsertArray(): array
	{
		return [
			'title' => $this->getTitle(),
			'content' => $this->getContent(),
			'written_by' => $this->getUserId()
		];
	}

	/**
	 * Method to return the entity as an array
	 * @return array
	 */
	public function toArray(): array
	{
		return (array)$this;
	}
}
