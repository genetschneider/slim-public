<?php

declare(strict_types=1);

namespace SlimApp\App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\PhpRenderer;

class AppController
{
	private $renderer;

	public function __construct(PhpRenderer $renderer)
	{
		$this->renderer = $renderer;
	}

	/**
	 * index view
	 * @param $request
	 * @param $response
	 * @return ResponseInterface
	 */
	public function index(Request $request, Response $response): ResponseInterface
	{
		return $this->renderer->render($response, 'index.phtml', []);
	}

	/**
	 * add post view
	 * @param $request
	 * @param $response
	 * @return ResponseInterface
	 */
	public function addPost(Request $request, Response $response): ResponseInterface
	{
		return $this->renderer->render($response, 'addPost.phtml', []);
	}
}
