<?php

declare(strict_types = 1);

namespace SlimApp\App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\PhpRenderer;
use SlimApp\Service\BlogUserService;

class UserController
{
	/** @var BlogUserService $blogUserService */
	private $blogUserService;

	/** @var PhpRenderer $renderer */
	private $renderer;

	public function __construct(BlogUserService $blogUserService, PhpRenderer $renderer)
	{
		$this->blogUserService = $blogUserService;
		$this->renderer = $renderer;
	}

	/**
	 * gets user by id
	 * @param Request $request
	 * @param Response $response
	 * @param array $args
	 * @return Response
	 */
	public function get(Request $request, Response $response, array $args): Response
	{
		$user = $this->blogUserService->get((int)$args['id']);

		return $response->withJson($user);
	}
}
