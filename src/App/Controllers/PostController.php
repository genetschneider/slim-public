<?php

declare(strict_types=1);

namespace SlimApp\App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\PhpRenderer;
use SlimApp\Service\BlogPostService;
use SlimApp\Service\BlogUserService;

class PostController
{
	const RESULT_KEY = 'result';

	/** @var BlogPostService $blogPostService */
	private $blogPostService;

	/** @var BlogUserService $blogUserService */
	private $blogUserService;

	/** @var PhpRenderer $renderer */
	private $renderer;

	public function __construct(BlogPostService $blogPostService, BlogUserService $blogUserService, PhpRenderer $renderer)
	{
		$this->blogPostService = $blogPostService;
		$this->blogUserService = $blogUserService;
		$this->renderer = $renderer;
	}

	/**
	 * renders view of all posts
	 * @param Request $request
	 * @param Response $response
	 * @return ResponseInterface
	 */
	public function getAll(Request $request, Response $response): ResponseInterface
	{
		$posts = $this->blogPostService->getAll();

		return $this->renderer->render($response, 'allPosts.phtml', [
			'posts' => $posts,
			self::RESULT_KEY => count($posts) > 0 ? '' : 'No posts!'
		]);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param array $args
	 * @return Response
	 */
	public function get(Request $request, Response $response, array $args): Response
	{
		$user = $this->blogPostService->get((int)$args['id']);

		return $response->withJson($user);
	}

	/**
	 * receives post data, and renders add post view
	 * @param Request $request
	 * @param Response $response
	 * @return ResponseInterface
	 */
	public function insert(Request $request, Response $response): ResponseInterface
	{
		$postParams = $request->getParsedBody();

		if (!$postParams || count($postParams) < 3) {
			return $this->renderer->render(
				$response->withStatus(302)->withHeader('Location', '/slim'), 'insert.phtml', [
					self::RESULT_KEY => 'Something went wrong :('
				]
			);
		}

		$userId = $this->blogUserService->insertAndReturnId(htmlspecialchars($postParams['name']));
		$insertPost = $this->blogPostService->insert(htmlspecialchars($postParams['title']), htmlspecialchars($postParams['content']), $userId);

		return $this->renderer->render(
			$response, 'addPost.phtml', [
				self::RESULT_KEY => $insertPost ? 'Added post!' : 'Something went wrong :('
			]
		);
	}
}
