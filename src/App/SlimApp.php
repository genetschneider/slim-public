<?php

declare(strict_types = 1);

namespace SlimApp\App;

use Slim\App;
use Slim\Container;
use SlimApp\App\Controllers\AppController;
use SlimApp\App\Controllers\PostController;
use SlimApp\App\Controllers\UserController;
use Illuminate\Database\Capsule\Manager;
use SlimApp\App\Models\Hydrator\PostHydrator;
use SlimApp\App\Models\Hydrator\UserHydrator;
use SlimApp\Service\BlogPostService;
use SlimApp\Service\BlogUserService;

class SlimApp extends App
{
	public function __construct($settings)
	{
		parent::__construct($settings);
	}

	/**
	 * register services
	 * @return SlimApp
	 */
	public function registerServices(): SlimApp
	{
		/**
		 * @var Container $container
		 */
		$container = $this->getContainer();

		$container['BlogUserService'] = function(Container $container) {
			$table = $container->get('db')->table('users');
			return new BlogUserService($table, new UserHydrator());
		};

		$container['BlogPostService'] = function(Container $container) {
			$table = $container->get('db')->table('posts');
			return new BlogPostService($table, new PostHydrator());
		};

		//db
		$container['db'] = function($container) {
			$capsule = new Manager;
			$capsule->addConnection($container['settings']['db']);

			$capsule->setAsGlobal();
			$capsule->bootEloquent();

			return $capsule;
		};

		return $this;
	}

	/**
	 * Register controllers
	 * @return SlimApp
	 */
	public function registerControllers(): SlimApp
	{
		/**
		 * @var Container $container
		 */
		$container = $this->getContainer();

		$container['UserController'] = function(Container $container) {
			$blogUserService = $container->get('BlogUserService');
			$renderer = $container->get('renderer');
			return new UserController($blogUserService, $renderer);
		};

		$container['PostController'] = function(Container $container) {
			$blogPostService = $container->get('BlogPostService');
			$blogUserService = $container->get('BlogUserService');
			$renderer = $container->get('renderer');
			return new PostController($blogPostService, $blogUserService, $renderer);
		};

		$container['AppController'] = function(Container $container) {
			$renderer = $container->get('renderer');
			return new AppController($renderer);
		};

		return $this;
	}
}
