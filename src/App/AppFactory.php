<?php

declare(strict_types=1);

namespace SlimApp\App;

use Slim\App;

final class AppFactory
{
	public static function createApp($settings): App
	{
		$app = new SlimApp($settings);
		$app->registerControllers()
			->registerServices();

		return $app;
	}
}
