<?php
// Routes

//main
$app->get('/', 'AppController:index');
$app->get('/add-post', 'AppController:addPost');

//post
$app->get('/all-posts', 'PostController:getAll');
$app->get('/post/{id}', 'PostController:get');
$app->post('/insert-post', 'PostController:insert');

//user
$app->get('/user-name/{name}', 'UserController:getByName');
$app->get('/user/{id}', 'UserController:get');
