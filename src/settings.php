<?php
return [
	'settings' => [
		'displayErrorDetails' => true, // set to false in production
		'addContentLengthHeader' => true, // Allow the web server to send the content-length header

		'db' => [
			'driver' => 'mysql',
			'host' => 'localhost',
			'database' => 'slim',
			'username' => 'root',
			'password' => 'root',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		],
		// Renderer settings
		'renderer' => [
			'template_path' => __DIR__ . '/../templates/',
		],

		'container' => new \Slim\Container,

		// Monolog settings
		'logger' => [
			'name' => 'slim-app',
			'path' => __DIR__ . '/../logs/app.log',
			'level' => \Monolog\Logger::DEBUG,
		],
	],
];
