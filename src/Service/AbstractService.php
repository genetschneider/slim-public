<?php

declare(strict_types = 1);

namespace SlimApp\Service;

abstract class AbstractService
{
	/**
	 * returns assoc array of json data
	 * @param $data
	 * @return array
	 */
	public function jsonToAssocArray($data): array
	{
		$data = $data ?? [];
		return json_decode(json_encode($data), true);
	}
}
