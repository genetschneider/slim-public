<?php

declare(strict_types = 1);

namespace SlimApp\Service;

use Illuminate\Database\Query\Builder;
use SlimApp\App\Models\Hydrator\UserHydrator;
use SlimApp\App\Models\User;

class BlogUserService extends AbstractService
{
	/** @var Builder $table */
	private $table;

	/** @var UserHydrator $hydrator */
	private $hydrator;

	public function __construct(Builder $table, UserHydrator $hydrator)
	{
		$this->table = $table;
		$this->hydrator = $hydrator;
	}

	/**
	 * gets user data (not entity) by id
	 * @param int $id
	 * @return array
	 */
	public function get(int $id): array
	{
		$userData = $this->table->get()->where('id', $id)->first();

		if (!$userData) {
			return [];
		}

		$user = $this->hydrator->hydrate((array)$userData);

		return $user->toArray();
	}

	/**
	 * checks if user exists in db, if not inserts it and returns its id
	 * @param string $name
	 * @return int
	 */
	public function insertAndReturnId(string $name): int
	{
		$userId = $this->getByNameAndReturnId($name);
		if ($userId == -1) {
			$user = new User($name);
			$userId = $this->table->insertGetId($user->getInsertArray());
		}

		return $userId;
	}

	/**
	 * gets user from db, returns -1 if not found
	 * @param string $name
	 * @return int
	 */
	protected function getByNameAndReturnId(string $name): int
	{
		$userData = $this->jsonToAssocArray($this->table->get()->where('name', $name)->first());

		if (!$userData) {
			return -1;
		}

		$user = new User($userData['name'], $userData['id']);
		return $user->getId();
	}
}
