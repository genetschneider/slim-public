<?php

declare(strict_types = 1);

namespace SlimApp\Service;

use Illuminate\Database\Query\Builder;
use SlimApp\App\Models\Hydrator\PostHydrator;
use SlimApp\App\Models\Post;

class BlogPostService extends AbstractService
{
	/** @var Builder $table*/
	private $table;

	/** @var PostHydrator $hydrator*/
	private $hydrator;

	public function __construct(Builder $table, PostHydrator $hydrator)
	{
		$this->table = $table;
		$this->hydrator = $hydrator;
	}

	/**
	 * Gets post by id
	 * @param int $id
	 * @return array
	 */
	public function get(int $id): array
	{
		$postArray = $this->table->get()->where('id', $id)->first();

		if (!$postArray) {
			return [];
		}

		$post = $this->hydrator->hydrate((array)$postArray);

		return $post->toArray();
	}

	/**
	 * gets all post data (not models)
	 * @return array
	 */
	public function getAll(): array
	{
		$posts = $this->table->join('users', 'users.id', '=', 'written_by')
			->orderBy('posts.id', 'desc')->get()->toArray();
		return $this->jsonToAssocArray($posts);
	}

	/**
	 * inserts post to db
	 * @param string $title
	 * @param string $content
	 * @param int $userId
	 * @return bool
	 */
	public function insert(string $title, string $content, int $userId): bool
	{
		$post = new Post($title, $content, $userId);
		return $this->table->insert($post->getInsertArray());
	}
}
